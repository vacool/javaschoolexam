package com.tsystems.javaschool.tasks.calculator;

public class BinaryOperator implements Comparable<BinaryOperator> {
    private static int counter;
    private final int id;
    private final char op;
    private final long prio;
    private double data;
    private BinaryOperator next;
    private BinaryOperator prev;

    BinaryOperator(char op, long prio, double data, BinaryOperator prev, BinaryOperator next) {
        this.op = op;
        this.prio = prio << 16 | MethodRepository.GetPriority(op);
        this.data = data;
        this.next = next;
        this.prev = prev;
        updateSiblings(this, this);

        this.id = BinaryOperator.counter++;
    }

    double getData() {
        return data;
    }

    char getOp() {
        return op;
    }

    void setData(double data) {
        this.data = data;
    }

    private void updateSiblings(BinaryOperator prev, BinaryOperator next) {
        if (this.prev != null) {
            this.prev.next = next;
        }
        if (this.next != null) {
            this.next.prev = prev;
        }
    }

    BinaryOperator extract() {
        updateSiblings(prev, next);
        this.next = null;
        this.prev = null;
        return this;
    }

    BinaryOperator getNext() {
        return next;
    }

    BinaryOperator getPrev() {
        return prev;
    }

    @Override
    public int compareTo(BinaryOperator o) {
        int result = Long.compare(this.prio, o.prio);
        if (result == 0) {
            result = o.id - this.id;
        }
        return -result;
    }
}
