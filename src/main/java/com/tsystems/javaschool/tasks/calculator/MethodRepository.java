package com.tsystems.javaschool.tasks.calculator;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public final class MethodRepository {
    private static final Map<Character, BiFunction<Double, Double, Double>> handlers = new HashMap<Character, BiFunction<Double, Double, Double>>();

    static {
        handlers.put('+', (a, b) -> a + b);
        handlers.put('-', (a, b) -> a - b);
        handlers.put('/', (a, b) -> a / b);
        handlers.put('*', (a, b) -> a * b);
        //handlers = Collections.unmodifiableMap(handlers);
    }

    private MethodRepository () { // private constructor

    }


    public static double InvokeBinary(Character operator, double arg1, double arg2)   {
        return handlers.get(operator).apply(arg1, arg2);
    }

    public static int GetPriority(Character operator) {
        switch (operator)
        {
            case '+':
            case '-':
                return 0;
            case '/':
            case '*':
                return 1;
            default:
                return -1;
        }
    }
}

