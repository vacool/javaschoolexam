package com.tsystems.javaschool.tasks.calculator;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;
import java.util.PriorityQueue;

public class Calculator {



    enum MathUnit {
        OpenParentheses, CloseParentheses, NaturalNumber, BinaryOperator, Nothing
    }

    private static final char stopChar = '?';

    private static final int size = MathUnit.values().length;
    private static final boolean[][] Map_ = new boolean[size][size];

    static {
        Map_[MathUnit.OpenParentheses.ordinal()][MathUnit.NaturalNumber.ordinal()] = true;
        Map_[MathUnit.OpenParentheses.ordinal()][MathUnit.OpenParentheses.ordinal()] = true;

        Map_[MathUnit.CloseParentheses.ordinal()][MathUnit.BinaryOperator.ordinal()] = true;
        Map_[MathUnit.CloseParentheses.ordinal()][MathUnit.CloseParentheses.ordinal()] = true;
        Map_[MathUnit.CloseParentheses.ordinal()][MathUnit.Nothing.ordinal()] = true;

        Map_[MathUnit.NaturalNumber.ordinal()][MathUnit.CloseParentheses.ordinal()] = true;
        Map_[MathUnit.NaturalNumber.ordinal()][MathUnit.BinaryOperator.ordinal()] = true;

        Map_[MathUnit.BinaryOperator.ordinal()][MathUnit.NaturalNumber.ordinal()] = true;
        Map_[MathUnit.BinaryOperator.ordinal()][MathUnit.OpenParentheses.ordinal()] = true;

        Map_[MathUnit.Nothing.ordinal()][MathUnit.NaturalNumber.ordinal()] = true;
        Map_[MathUnit.Nothing.ordinal()][MathUnit.OpenParentheses.ordinal()] = true;
        // rules = Collections.unmodifiableMap(rules);

    }
    private static final DecimalFormat fmt = new DecimalFormat("", new DecimalFormatSymbols(Locale.ENGLISH));

    private static BinaryOperator split(String s) {
        final String operators = stopChar + "*/+-()";
        s += stopChar;

        BinaryOperator last = new BinaryOperator('\0', 0, 0, null, null);
        final BinaryOperator root = last;

        MathUnit curUnit = MathUnit.Nothing;
        MathUnit prevUnit = curUnit;

        int i = 0;
        int j = 0;
        int prio = 0;
        double d = 0;

        do {
            while (operators.indexOf(s.charAt(j)) == -1) { j++; }

            if	(j - i != 0) {
                try {
                    d = fmt.parse(s.substring(i, j)).doubleValue();
                    curUnit = MathUnit.NaturalNumber;
                } catch (ParseException e) {
                    e.printStackTrace();
                    return null;
                }
            } else {
                char c = s.charAt(j);
                if	(c == ')') {
                    --prio;
                    if (prio < 0)
                        return null;
                    curUnit = MathUnit.CloseParentheses;
                }
                else if	(c == '(') {
                    ++prio;
                    curUnit = MathUnit.OpenParentheses;
                }
                else  {
                    curUnit = MathUnit.BinaryOperator;
                }
                ++j;
            }

            i = j;
            if	(!Map_[prevUnit.ordinal()][curUnit.ordinal()])
                return null;
            if	(curUnit == MathUnit.BinaryOperator)
            {
                char op = s.charAt(j - 1);
                last = new BinaryOperator(op, prio, d, last, null);
                //System.out.println(s.charAt(j - 1));
            }

            prevUnit = curUnit;
        } while (s.length() > j);

        if (prio != 0)
            return null;
        return root;
    }

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement ==  null || statement.isEmpty())
            return null;

        if (!statement.matches("(([1-9]+[0-9]*(\\.[0-9]+)?)|[*/\\-+)(])+"))
            return null;

        PriorityQueue<BinaryOperator> q = new PriorityQueue<BinaryOperator>();
        BinaryOperator root = split(statement);

        if (root == null || root.getNext() == null)
            return null;

        BinaryOperator item = root.getNext();
        while (item != null) {
            q.add(item);
            item = item.getNext();
        }

        do {
            item = q.poll();
            if (item.getOp() == stopChar)
                break;
            //if (item == null)
            //break;
            BinaryOperator next = item.getNext();
            next.setData(MethodRepository.InvokeBinary(item.getOp(), item.getData(), next.getData()));
            if (Double.isInfinite(next.getData()))
                return null;
            item.extract();
        } while (true);

        String s = ((Double)item.getData()).toString();
        return !s.contains(".") ? s : s.replaceAll("0*$", "").replaceAll("\\.$", "");
    }

}
