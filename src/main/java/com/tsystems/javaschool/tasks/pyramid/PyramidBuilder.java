package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        //number of rows is arithmetic progression
        //solution is% (-1 +- sqrt(1+8*x)) / 2
        if (inputNumbers == null || inputNumbers.size() == 0)
            throw new CannotBuildPyramidException();
        double n = (Math.sqrt(1 + inputNumbers.size() * 8) - 1) / 2;
        if ((int)n != n || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        List<Integer> tmp = inputNumbers.stream().sorted().collect(Collectors.toList());

        int dim = (int)n;
        int [][] result = new int[dim][2 * dim - 1];
        int row = 1;
        int counter = 0;
        for (Integer i : tmp) {
            if (counter == row)
            {
                counter = 0;
                ++row;
            }
            result[row -  1][dim - row + counter*2] = i;
            ++counter;
        }
        return result;
    }


}
